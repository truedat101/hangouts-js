'use strict';

var bot = require('./index');

bot.on('message', function(from, message, deviceId) {
  console.log(from + " (" + deviceId + ") said : " + message);
  bot.sendMessage(from, "I received your message.");
});

bot.on('typing', function(from, status) {

  switch (status) {
    case "typing":
      console.log(from, "is typing...");
      break;
    case "paused":
      console.log(from, "has typed something.");
      break;
    case "clear-typing":
      console.log("[clear typing text from " + from + "]");
      break;
  }
});

bot.on('status-changed', function(from, status, customStatus, capabilities, photo) {

  console.log(from, photo ? ('[' + photo + ']') : '' + 'changed status to "' + status + '" ' + (customStatus ? '(' + customStatus + ') ' : '') + 'with', capabilities.length ? capabilities : 'no', 'capabilities');
});

bot.on('image', function(from, filename, image, thumbnail, size, message) {

  console.log(from, "is sending", image, "image with", size,"bytes size (thumbnail : " + thumbnail + ") " + (filename ? "named " + filename : "") + "and saying", message ? ": " + message : 'nothing');
});

bot.on('friend-request', function(from, nickName) {

  console.log(from, (nickName ? '"' + nickName + '" ' : '') + "is requesting to be your friend");
});

bot.on('friended', function(from) {

  console.log(from, "is now your friend");
});

bot.login('your.username@gmail.com', 'your.password', true, function() {
  console.log('I\'m up');
});
