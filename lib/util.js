'use strict';

var vm = require('vm');

module.exports.evalInSandbox = function evalInSandbox(text) {
  var sandbox = {
    window: {
    },
    document: {
    }
  };

  vm.runInNewContext(text, sandbox, 'google-bgresponse');

  return sandbox;
};
