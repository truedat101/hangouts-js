'use strict';

/*!
 * Hangouts
 * Copyright(c) 2015 Masih Yeganeh <masihyeganeh@outlook.com>
 * MIT Licensed
 *
 * GitHub: https://github.com/masihyeganeh/hangouts-js
 * NPM: http://npmjs.org/package/hangouts
 * Travis CI: https://travis-ci.org/masihyeganeh/hangouts-js
 */

module.exports = require('./lib/messenger');
